# SE420-EvaP Course Evaluation


## Description
EvaP is an online platform being used for evaluation of university courses. It aims at helping to improve the quality of teaching by providing feedback on courses to lecturers and university management.

Students are provided with a website to anonymously complete questionnaires for the courses in which they are enrolled. After publishing the aggregated results of the questionnaires are accessible for students, lecturers and university management on the same platform.

EvaP was developed at the Hasso Plattner Institute (HPI) in Potsdam, Germany, where it replaced the former used EvaJ in 2011, which became unmaintainable.

We are a group of students from Drexel College of Computing and Informatics, taking SE 420 (Open Source Software Engineering). Our task is to analyze the GitHub repository for EvaP course evaluation project and provide any contributions towards this open sourced project.


## Authors

Asad Ali

Akshay Vaswani


## Project status
We are at the initial stage of analyzing EvaP contents and have started making any contributions where we can. We will proceed to update this section as as soon as there is progress in our project's status.
